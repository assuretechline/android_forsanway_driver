package com.forsanway.driver.api;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jacksonandroidnetworking.JacksonParserFactory;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;


import org.json.JSONObject;

import java.util.Properties;


public class APIServer {

    Context context;
    Properties properties;
    String subUrl = "";
    AppPrefrece appPrefrece;
    String mainUrl = "";


    public APIServer(Context context) {
        this.context = context;
        appPrefrece = new AppPrefrece(context);
        properties = new LoadAssetProperties().loadRESTApiFile(context.getResources(), "rest.properties", context);
        mainUrl = properties.getProperty("MainUrl");
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
    }

    public void signupApi(final APIResponse listener, String name, String email, String password, String password_confirmation, String mobile) {
        subUrl = properties.getProperty("singupAPI");
        AndroidNetworking.post(mainUrl + subUrl)
                //.addHeaders("Auth-Key", GlobalAppConfiguration.auth)
                //.addHeaders("Client-Service",GlobalAppConfiguration.client)
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .addBodyParameter("password_confirmation", password_confirmation)
                .addBodyParameter("mobile", mobile)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        // do anything with response
                        //getSuccessListener(message, listener);
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        //getErrorListener(message, listener);
                        listener.onFailure(error.getErrorBody());
                    }
                });
    }

    public void loginApi(final APIResponse listener, String email, String password) {
        subUrl = properties.getProperty("loginAPI");
        AndroidNetworking.post(mainUrl + subUrl)
                //.addHeaders("Auth-Key", GlobalAppConfiguration.auth)
                //.addHeaders("Client-Service",GlobalAppConfiguration.client)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .addBodyParameter("remember_me", "1")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getErrorBody());
                    }
                });
    }

    public void getTrip(final APIResponse listener, String user_id) {
        subUrl = properties.getProperty("trips");
        AndroidNetworking.get(mainUrl + subUrl)
                //.addHeaders("Auth-Key", GlobalAppConfiguration.auth)
                //.addHeaders("Client-Service",GlobalAppConfiguration.client)
                .addPathParameter("user_id", user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        listener.onFailure(error.getErrorBody());
                    }
                });
    }

    public void Get_Emp_Status(final APIResponse listener, String Empid) {
        subUrl = properties.getProperty("Emp_Status") + Empid;

        AndroidNetworking.get(mainUrl + subUrl)
                //.addHeaders("Auth-Key", GlobalAppConfiguration.auth)
                //.addHeaders("Client-Service",GlobalAppConfiguration.client)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onFailure(anError.getErrorBody());
                    }
                });
    }

    public void attandanceList(final APIResponse listener, String class_id) {
        subUrl = properties.getProperty("checkAtt");


        AndroidNetworking.post(mainUrl + subUrl)
                //.addHeaders("Auth-Key", GlobalAppConfiguration.auth)
                //.addHeaders("Client-Service",GlobalAppConfiguration.client)
                .addBodyParameter("class_id", class_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject object) {
                        // do anything with response
                        //getSuccessListener(message, listener);
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        //getErrorListener(message, listener);
                        listener.onFailure(error.getErrorBody());
                    }
                });
    }

}
