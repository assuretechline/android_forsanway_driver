package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.forsanway.driver.R;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.ogaclejapan.smarttablayout.SmartTabLayout;


/**
 * Created by home on 14/07/18.
 */


public class WithdrawFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ViewPager vpgr_withdraw;
    ViewPagerAdapter myCustomPagerAdapter;
    CreatePaymentFragment createPaymentFragment;
    RequestListFragment requestListFragment;
    SmartTabLayout smartTabLayout;
    @SuppressLint("ValidFragment")
    public WithdrawFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_withdraw, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView(View view) {
        smartTabLayout = view.findViewById(R.id.smartTabLayout);
        vpgr_withdraw = view.findViewById(R.id.vpgr_withdraw);
    }

    private void setData() {
        myCustomPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        vpgr_withdraw.setAdapter(myCustomPagerAdapter);
        smartTabLayout.setViewPager(vpgr_withdraw);
    }

    private void setListener() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        String[] title={context.getResources().getString(R.string.Create),context.getResources().getString(R.string.AcceptRequestList),context.getResources().getString(R.string.PendingRequestList)};

        public ViewPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                createPaymentFragment = new CreatePaymentFragment();
                return createPaymentFragment;
            }else if(position==1) {
                requestListFragment = new RequestListFragment();
                return requestListFragment;
            }else {
                requestListFragment = new RequestListFragment();
                return requestListFragment;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }

        @Override
        public int getCount() {
            return title.length;
        }
    }
}