package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.AddImageListingAdapter;
import com.forsanway.driver.adapter.AminityRecycleAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.GlobalAppConfiguration;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.AddImageListingData;
import com.forsanway.driver.model.MenuData;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * Created by home on 14/07/18.
 */


public class AddCarFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    ArrayList<MenuData> aminityArrayList = new ArrayList<>();
    RecyclerView rv_aminity,rv_media;
    AminityRecycleAdapter aminityRecycleAdapter;
    ImageView imgv_minus_person, imgv_plus_person, imgv_minus_bag, imgv_plus_bag,imgv_media;
    ArrayList<AddImageListingData> addImageListingData = new ArrayList<>();
    String final_path = "";
    AddImageListingAdapter addImageListingAdapter;
    TextView txt_count_person,txt_count_bag;

    @SuppressLint("ValidFragment")
    public AddCarFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_add_car, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
        if (!Utility.verifyStoragePermissions(context)) {
            Utility.checkAndRequestPermissions(context);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        rv_aminity = view.findViewById(R.id.rv_aminity);
        rv_media = view.findViewById(R.id.rv_media);
        imgv_minus_person = view.findViewById(R.id.imgv_minus_person);
        imgv_plus_person = view.findViewById(R.id.imgv_plus_person);
        imgv_minus_bag = view.findViewById(R.id.imgv_minus_bag);
        imgv_plus_bag = view.findViewById(R.id.imgv_plus_bag);
        imgv_media = view.findViewById(R.id.imgv_media);
        txt_count_bag = view.findViewById(R.id.txt_count_bag);
        txt_count_person = view.findViewById(R.id.txt_count_person);
    }

    private void setData() {
        aminityArrayList.add(new MenuData("", R.drawable.f1, false));
        aminityArrayList.add(new MenuData("", R.drawable.f2, false));
        aminityArrayList.add(new MenuData("", R.drawable.f3, false));
        aminityArrayList.add(new MenuData("", R.drawable.f4, false));
        aminityArrayList.add(new MenuData("", R.drawable.f5, false));

        //rv_aminity.setHasFixedSize(true);
        aminityRecycleAdapter = new AminityRecycleAdapter(context, aminityArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rv_aminity.setLayoutManager(linearLayoutManager);
        rv_aminity.setAdapter(aminityRecycleAdapter);

        addImageListingAdapter = new AddImageListingAdapter(context, addImageListingData);
        rv_media.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_media.setAdapter(addImageListingAdapter);

    }

    private void setListener() {
        imgv_minus_person.setOnClickListener(this);
        imgv_plus_person.setOnClickListener(this);
        imgv_minus_bag.setOnClickListener(this);
        imgv_plus_bag.setOnClickListener(this);
        imgv_media.setOnClickListener(this);
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == imgv_minus_person) {
            if(!txt_count_person.getText().toString().equals("1")){
                txt_count_person.setText((Integer.parseInt(txt_count_person.getText().toString())-1)+"");
            }
        } else if (view == imgv_plus_person) {
            txt_count_person.setText((Integer.parseInt(txt_count_person.getText().toString())+1)+"");
        } else if (view == imgv_minus_bag) {
            if(!txt_count_bag.getText().toString().equals("1")){
                txt_count_bag.setText((Integer.parseInt(txt_count_bag.getText().toString())-1)+"");
            }
        } else if (view == imgv_plus_bag) {
            txt_count_bag.setText((Integer.parseInt(txt_count_bag.getText().toString())+1)+"");
        }else if (view == imgv_media) {
            imgFromGallry();
        }
    }

    private void imgFromGallry() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1001);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Log.e("Tag","RESULT_OK");
            if (requestCode == 1001) {
                Log.e("Tag","requestCode");
                if (data.getClipData() != null) {
                    int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                    Log.e("Tag","count "+count);
                    for (int i = 0; i < count; i++) {
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        String path = null;
                        try {
                            path = Utility.getRealPathFromURI(context,imageUri);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                            Log.e("Tag",imageUri + " * URISyntaxException "+e.getMessage());
                        }
                        try {
                            addImageListingData.add(0, new AddImageListingData(
                                    "",
                                    "" + path,
                                    "file://" + new File(path),
                                    false));

                            //Log.e("Tag", "Data path  : " + path);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Tag","URISyntaxException "+e.getMessage());
                        }
                        //do something with the image (save it to some directory or whatever you need to do with it here)
                    }
                } else if (data.getData() != null) {
                    //Log.e("Tag","Image bottom : "+data.getData().getPath());
                    String path = null;
                    try {
                        path = Utility.getRealPathFromURI(context, data.getData());
                        if (path != null) {
                            final_path = path;
                            addImageListingData.add(0, new AddImageListingData(
                                    "",
                                    "" + path,
                                    "file://" + new File(path),
                                    false));
                        }
                        //Log.e("Tag", "Data path  : " + path);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    //do something with the image (save it to some directory or whatever you need to do with it here)
                }
                addImageListingAdapter.notifyDataSetChanged();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.e("Tag","RESULT_CANCELED");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}