package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.Calendar;


/**
 * Created by home on 14/07/18.
 */


public class AddTripFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    TextView txt_create, txt_delete, txt_date, txt_time;
    CheckBox chb_ride, chb_shipment;
    LinearLayout ll_date, ll_time;

    @SuppressLint("ValidFragment")
    public AddTripFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_add_trip, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        txt_create = view.findViewById(R.id.txt_create);
        txt_delete = view.findViewById(R.id.txt_delete);
        txt_date = view.findViewById(R.id.txt_date);
        txt_time = view.findViewById(R.id.txt_time);
        chb_ride = view.findViewById(R.id.chb_ride);
        chb_shipment = view.findViewById(R.id.chb_shipment);
        ll_date = view.findViewById(R.id.ll_date);
        ll_time = view.findViewById(R.id.ll_time);
    }

    private void setData() {

    }

    private void setListener() {
        txt_create.setOnClickListener(this);
        ll_date.setOnClickListener(this);
        ll_time.setOnClickListener(this);

        chb_ride.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    chb_shipment.setChecked(false);
                } else {
                    chb_shipment.setChecked(true);
                }
            }
        });

        chb_shipment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    chb_ride.setChecked(false);
                } else {
                    chb_ride.setChecked(true);
                }
            }
        });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == txt_create) {

        } else if (view == ll_date) {

            final Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog StartTime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    txt_date.setText(Utility.setDateShortForVisit(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth + ""));
                }
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            StartTime.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            StartTime.show();

        } else if (view == ll_time) {

            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    txt_time.setText(selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }
    }

}