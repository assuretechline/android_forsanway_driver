package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.CarListAdapter;
import com.forsanway.driver.adapter.PaymentHistoryAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class PaymentHistoryFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    ListView lst_car;
    PaymentHistoryAdapter paymentHistoryAdapter;
    ArrayList<CarListData> carListDataArrayList=new ArrayList<>();

    @SuppressLint("ValidFragment")
    public PaymentHistoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_payment_history, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        lst_car = view.findViewById(R.id.lst_car);
    }

    private void setData() {
        carListDataArrayList.add(new CarListData("Mohammad","(Dubai to Doha)","https://dvh1deh6tagwk.cloudfront.net/money-transfers/images/product/paypallogo-supplied-310x194.png?ver=20190902-125006","05","04","F 07732","06 July 2020",true));
        carListDataArrayList.add(new CarListData("Mohammad","(Dubai to Doha)","https://www.bankindia.org/wp-content/uploads/2017/07/SBI-atm-card-erased.jpg","03","02","G 88456","06 July 2020",false));
        carListDataArrayList.add(new CarListData("Mohammad","(Dubai to Doha)","https://dvh1deh6tagwk.cloudfront.net/money-transfers/images/product/paypallogo-supplied-310x194.png?ver=20190902-125006","03","02","G 88456","06 July 2020",false));

        paymentHistoryAdapter=new PaymentHistoryAdapter(context,carListDataArrayList);
        lst_car.setAdapter(paymentHistoryAdapter);
    }

    private void setListener() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }
}