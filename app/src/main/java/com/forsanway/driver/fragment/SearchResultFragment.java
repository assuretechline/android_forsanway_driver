package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.CarListAdapter;
import com.forsanway.driver.adapter.MyCustomPagerAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.MenuData;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by home on 14/07/18.
 */


public class SearchResultFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;
    ViewPager vpgr_image;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    ArrayList<String> images = new ArrayList<>();
    MyCustomPagerAdapter myCustomPagerAdapter;
    PageIndicatorView wormDotsIndicator;
    ListView lst_car;
    ArrayList<MenuData> carListData=new ArrayList<>();


    @SuppressLint("ValidFragment")
    public SearchResultFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_search_reasult, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        vpgr_image = view.findViewById(R.id.vpgr_image);
        wormDotsIndicator = view.findViewById(R.id.pageIndicatorView);
        lst_car = view.findViewById(R.id.lst_car);
    }

    private void setData() {
        setViewpagerData();

    }

    private void setListener() {
        vpgr_image.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
      /*  if (view == imgv_menu) {
            //MainActivity.getInstance().setDrawer();
        }*/
    }

    private void setViewpagerData(){
        images.add("https://images.pexels.com/photos/241316/pexels-photo-241316.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        images.add("https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/mercedes_benz_660_050120090507.jpg");
        images.add("https://images.pexels.com/photos/241316/pexels-photo-241316.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        images.add("https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/mercedes_benz_660_050120090507.jpg");

        NUM_PAGES = images.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                vpgr_image.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 4000);

        myCustomPagerAdapter = new MyCustomPagerAdapter(context, images);
        vpgr_image.setAdapter(myCustomPagerAdapter);
        wormDotsIndicator.setViewPager(vpgr_image);
    }

}