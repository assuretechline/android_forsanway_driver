package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.activity.MainActivity;
import com.forsanway.driver.adapter.CarListAdapter;
import com.forsanway.driver.adapter.TripListAdapter;
import com.forsanway.driver.api.APIResponse;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.MyPreference;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;

import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class TripListFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    TextView txt_new_trip;
    ListView lst_car;
    TripListAdapter tripListAdapter;
    ArrayList<CarListData> carListDataArrayList=new ArrayList<>();
    ProgressDialog pd;

    @SuppressLint("ValidFragment")
    public TripListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_trip_list, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        lst_car = view.findViewById(R.id.lst_car);
        txt_new_trip = view.findViewById(R.id.txt_new_trip);
    }

    private void setData() {
        carListDataArrayList.add(new CarListData("#BK001234","Travel Booking and Shipment","https://i.ndtvimg.com/i/2016-12/2017-suzuki-swift_827x510_61482909174.jpg","Katar to Doha","04","Flat no 20, Royal Height","06 June 2020",true));
        carListDataArrayList.add(new CarListData("#BK001235","Travel Booking","https://s3.india.com/auto/wp-content/uploads/2016/11/cd559170d6fd513d9bd285c5f366ab1e_251X188_1.jpg","Katar to Doha","02","Flat no 20, Royal Height","06 June 2020",false));
        carListDataArrayList.add(new CarListData("#BK001236","Shipment","https://cars.usnews.com/static/images/Auto/izmo/i130355861/2020_mercedes_benz_gle_angularfront.jpg","Katar to Doha","02","Flat no 20, Royal Height","06 June 2020",false));

        tripListAdapter=new TripListAdapter(context,carListDataArrayList);
        lst_car.setAdapter(tripListAdapter);

        if (Utility.isNetworkAvailable(context))
            getTrip();
        else
            Utility.errDialog(context.getResources().getString(R.string.network), context);

    }

    private void setListener() {

        txt_new_trip.setOnClickListener(this);

        lst_car.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.getInstance().setFragmentMainTab(new TripDetailFragment(),"TripDetailFragment");
            }
        });
    }

    private void setColor() {

    }

    private void getTrip(){
        pd = Utility.showProgressDialog(context);
        apiServer.getTrip(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag -> ", "object -> " + object.toString());
                    Utility.dismissProgressDialog(pd);

                } catch (Exception e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);

            }
        },  MyPreference.getPreferenceValue(context,"user_id").toString());
    }

    @Override
    public void onClick(View view) {
        if(view==txt_new_trip){
            MainActivity.getInstance().setFragmentMainTab(new AddTripFragment(),"AddTripFragment");
        }
    }
}