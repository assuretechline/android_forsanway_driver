package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


/**
 * Created by home on 14/07/18.
 */


public class CreatePaymentFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    LinearLayout ll_payment_type;
    TextView txt_payment_type,txt_amount,txt_create,txt_delete;


    @SuppressLint("ValidFragment")
    public CreatePaymentFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_create_payment, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        ll_payment_type=view.findViewById(R.id.ll_payment_type);
        txt_amount=view.findViewById(R.id.txt_amount);
        txt_create=view.findViewById(R.id.txt_create);
        txt_delete=view.findViewById(R.id.txt_delete);
        txt_payment_type=view.findViewById(R.id.txt_payment_type);
    }

    private void setData() {

    }

    private void setListener() {
        txt_delete.setOnClickListener(this);
        txt_create.setOnClickListener(this);
        ll_payment_type.setOnClickListener(this);

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }

}