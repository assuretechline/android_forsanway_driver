package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.AminityRecycleAdapter;
import com.forsanway.driver.adapter.DashboardAdapter;
import com.forsanway.driver.adapter.MenuAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.DashboardData;
import com.forsanway.driver.model.MenuData;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.google.android.material.navigation.NavigationView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by home on 14/07/18.
 */


public class HomeFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ImageLoader imageLoader;

    ImageView imgv_amount_icon;
    ArrayList<MenuData> menuDataList = new ArrayList<>();
    GridView gv_top;
    FrameLayout fl_main;
    ProgressDialog pd;
    DashboardAdapter dashboardAdapter;
    ArrayList<DashboardData> dashboardDataArrayList = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_home, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView(View view) {
        imgv_amount_icon = view.findViewById(R.id.imgv_amount_icon);

        gv_top = view.findViewById(R.id.gv_top);
    }

    private void setData() {

        dashboardDataArrayList.add(new DashboardData(context.getString(R.string.TotalTrip), "10", R.drawable.dash_car, context.getResources().getColor(R.color.menu_color_1)));
        dashboardDataArrayList.add(new DashboardData(context.getString(R.string.TotalSale), "100", R.drawable.dash_sales, context.getResources().getColor(R.color.menu_color_2)));
        dashboardDataArrayList.add(new DashboardData(context.getString(R.string.TripBooking), "2418", R.drawable.dash_booking, context.getResources().getColor(R.color.menu_color_3)));
        dashboardDataArrayList.add(new DashboardData(context.getString(R.string.ShipBooking), "0", R.drawable.dash_tracking, context.getResources().getColor(R.color.menu_color_4)));

        dashboardAdapter = new DashboardAdapter(context, dashboardDataArrayList);
        gv_top.setAdapter(dashboardAdapter);
        Utility.setGridViewHeightBasedOnChildren(gv_top, 2);
    }

    private void setListener() {

    }

    private void setColor() {
        imgv_amount_icon.setColorFilter(context.getResources().getColor(R.color.white_background));
    }

    @Override
    public void onClick(View view) {
    }

}