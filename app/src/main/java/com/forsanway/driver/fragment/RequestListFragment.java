package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.CarListAdapter;
import com.forsanway.driver.adapter.RequestListAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class RequestListFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    TextView txt_add_car;
    ListView lst_car;
    RequestListAdapter requestListAdapter;
    ArrayList<CarListData> carListDataArrayList=new ArrayList<>();

    @SuppressLint("ValidFragment")
    public RequestListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_request_list, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        lst_car = view.findViewById(R.id.lst_car);
        txt_add_car = view.findViewById(R.id.txt_add_car);
    }

    private void setData() {
        carListDataArrayList.add(new CarListData("15 June 2020","","","","","$2800","Paypal",true));
        carListDataArrayList.add(new CarListData("12 June 2020","","","","","$2800","Paypal",false));
        carListDataArrayList.add(new CarListData("05 June 2020","","","","","$2800","Paypal",false));

        requestListAdapter=new RequestListAdapter(context,carListDataArrayList);
        lst_car.setAdapter(requestListAdapter);
    }

    private void setListener() {

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }
}