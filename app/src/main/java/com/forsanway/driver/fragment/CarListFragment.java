package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.activity.MainActivity;
import com.forsanway.driver.adapter.CarListAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class CarListFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    TextView txt_add_car;
    ListView lst_car;
    CarListAdapter carListAdapter;
    ArrayList<CarListData> carListDataArrayList=new ArrayList<>();

    @SuppressLint("ValidFragment")
    public CarListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_car_list, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        lst_car = view.findViewById(R.id.lst_car);
        txt_add_car = view.findViewById(R.id.txt_add_car);
    }

    private void setData() {
        carListDataArrayList.add(new CarListData("Maruti","White Swift","https://i.ndtvimg.com/i/2016-12/2017-suzuki-swift_827x510_61482909174.jpg","05","04","F 07732","Wifi, AC, Songs Playlist",true));
        carListDataArrayList.add(new CarListData("Mercedes","White C Class","https://s3.india.com/auto/wp-content/uploads/2016/11/cd559170d6fd513d9bd285c5f366ab1e_251X188_1.jpg","03","02","G 88456","Wifi, AC, Songs Playlist",false));
        carListDataArrayList.add(new CarListData("Mercedes","White SUV","https://cars.usnews.com/static/images/Auto/izmo/i130355861/2020_mercedes_benz_gle_angularfront.jpg","03","02","G 88456","Wifi, AC, Songs Playlist",false));

        carListAdapter=new CarListAdapter(context,carListDataArrayList);
        lst_car.setAdapter(carListAdapter);
    }

    private void setListener() {
        txt_add_car.setOnClickListener(this);

        lst_car.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //MainActivity.getInstance().setFragmentMainTab(new Car);
            }
        });

    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if(view==txt_add_car){
            MainActivity.getInstance().setFragmentMainTab(new AddCarFragment(),"AddCarFragment");
        }
    }
}