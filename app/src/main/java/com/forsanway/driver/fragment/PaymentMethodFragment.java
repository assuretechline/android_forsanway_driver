package com.forsanway.driver.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.PaymentAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;
import com.forsanway.driver.model.MenuData;

import java.util.ArrayList;


/**
 * Created by home on 14/07/18.
 */


public class PaymentMethodFragment extends Fragment implements View.OnClickListener {
    View view;
    Context context;
    APIServer apiServer;
    TextView txt_connect_method;
    GridView gv_method;
    PaymentAdapter paymentAdapter;
    ArrayList<MenuData> paymentArraylist=new ArrayList<>();

    @SuppressLint("ValidFragment")
    public PaymentMethodFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.fragment_payment_method, null, false);
        this.setHasOptionsMenu(true);
        initialize();
        setView(view);
        setData();
        setListener();
        setColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    private void initialize() {
        context = getContext();
        Utility.crashLytics(context);
        apiServer = new APIServer(context);
    }

    private void setView(View view) {
        gv_method = view.findViewById(R.id.gv_method);
        txt_connect_method = view.findViewById(R.id.txt_connect_method);
    }

    private void setData() {

        paymentArraylist.add(new MenuData("By Bank",R.drawable.card,true));
        paymentArraylist.add(new MenuData("By Google",R.drawable.google,false));
        paymentArraylist.add(new MenuData("By Paypal",R.drawable.paypal,false));
        paymentArraylist.add(new MenuData("By Cash",R.drawable.bycash,false));

        paymentAdapter=new PaymentAdapter(context,paymentArraylist);
        gv_method.setAdapter(paymentAdapter);
    }

    private void setListener() {
     gv_method.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             for(int j=0;j<paymentArraylist.size();j++){
                 paymentArraylist.get(j).setSelected(false);
             }
             paymentArraylist.get(position).setSelected(true);
             paymentAdapter.notifyDataSetChanged();
         }
     });
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {

    }
}