package com.forsanway.driver.model;

public class AddImageListingData {
    String id,imgPath,imgLoad;
    boolean isSelected;

    public AddImageListingData(String id, String imgPath, String imgLoad, boolean isSelected) {
        this.id = id;
        this.imgPath = imgPath;
        this.imgLoad = imgLoad;
        this.isSelected = isSelected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgLoad() {
        return imgLoad;
    }

    public void setImgLoad(String imgLoad) {
        this.imgLoad = imgLoad;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
