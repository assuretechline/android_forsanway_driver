package com.forsanway.driver.model;

/**
 * Created by Amisha on 21-Feb-18.
 */

public class CarListData {
    String title,model,image,person,bag,number,service;
    boolean isSelected;

    public CarListData(String title, String model, String image, String person, String bag, String number, String service, boolean isSelected) {
        this.title = title;
        this.model = model;
        this.image = image;
        this.person = person;
        this.bag = bag;
        this.number = number;
        this.service = service;
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getBag() {
        return bag;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
