package com.forsanway.driver.model;

/**
 * Created by Amisha on 21-Feb-18.
 */

public class DashboardData {
    String title,count;
    int image,color;

    public DashboardData(String title, String count, int image, int color) {
        this.title = title;
        this.count = count;
        this.image = image;
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
