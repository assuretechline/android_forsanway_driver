package com.forsanway.driver.global;


import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

public class MyPreference {
    public static final String True = "True";
    public static final String False = "False";

    public static Object getPreferenceValue(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        Map<String, ?> keys = pref.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            if (entry.getKey().equals(key))
                return entry.getValue();
        }
        return null;
    }

    public static void setPreferenceValue(Context context, String key, Object value) {
        SharedPreferences pref = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        if (value instanceof String)
            edit.putString(key, value.toString());
        if (value instanceof Integer)
            edit.putInt(key, (Integer) value);
        if (value instanceof Long)
            edit.putLong(key, (Long) value);
        if (value instanceof Float)
            edit.putFloat(key, (Float) value);
        if (value instanceof Boolean)
            edit.putBoolean(key, (Boolean) value);
        if (value instanceof Float)
            edit.putFloat(key, (Float) value);
        edit.commit();
    }

    public static void deletePreferece(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        pref.edit().remove(key).commit();
    }
}
