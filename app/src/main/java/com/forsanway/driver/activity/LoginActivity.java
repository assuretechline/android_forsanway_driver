package com.forsanway.driver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.forsanway.driver.R;
import com.forsanway.driver.api.APIResponse;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.MyPreference;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Properties;

/**
 * Created by home on 12/07/18.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ProgressDialog pd;
    ImageView imgv_logo, imgv_back;
    EditText edt_email_address, edt_password;
    TextView txt_login, txt_signup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
        //checkAndRequestPermissions();
    }

    private void initialize() {
        context = this;
        Utility.crashLytics(context);
        Utility.setStatusColor(this);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView() {
        imgv_logo = findViewById(R.id.imgv_logo);
        imgv_back = findViewById(R.id.imgv_back);

        edt_email_address = findViewById(R.id.edt_email_address);
        edt_password = findViewById(R.id.edt_password);

        txt_login = findViewById(R.id.txt_login);
        txt_signup = findViewById(R.id.txt_signup);

    }

    private void setData() {

    }

    private void setLitionar() {
        txt_login.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == txt_login) {
            if (Utility.isNetworkAvailable(context))
                checkValidation();
            else
                Utility.errDialog(context.getResources().getString(R.string.network), context);
        } else if (view == txt_signup) {
            Utility.gotoNext(context,SignupActivity.class);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void checkValidation() {
        String error = "";

        if (edt_email_address.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_email);
        } else if (!isValidEmail(edt_email_address.getText())) {
            error += "\n" + context.getString(R.string.enter_valid_email);
        } else if (edt_password.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_pass);
        }

        if (error.equals("")) {
            Utility.hideSoftKeyboard(edt_email_address,context);
            callLogin();
        } else {
            Utility.errDialog(error.substring(1), context);
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void callLogin() {
        pd = Utility.showProgressDialog(context);
        apiServer.loginApi(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag -> ", "object -> " + object.toString());
                    MyPreference.setPreferenceValue(context,"loginData",object.toString());
                    Utility.dismissProgressDialog(pd);

                    MyPreference.setPreferenceValue(context,"user_id","3");

                    Intent i = new Intent(context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                    overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
                    finish();

                } catch (Exception e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);

            }
        }, edt_email_address.getText().toString().trim(), edt_password.getText().toString().trim());
    }

}
