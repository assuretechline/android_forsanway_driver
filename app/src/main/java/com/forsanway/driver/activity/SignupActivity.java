package com.forsanway.driver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.forsanway.driver.R;
import com.forsanway.driver.api.APIResponse;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by home on 12/07/18.
 */

public class SignupActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    APIServer apiServer;
    AppPrefrece appPrefrece;
    ProgressDialog pd;
    ImageView imgv_logo, imgv_back;
    EditText edt_name, edt_email_address, edt_phone, edt_password, edt_confirm_password;
    TextView txt_login, txt_signup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = this;
        initialize();
        setView();
        setData();
        setLitionar();
        setColor();
        //checkAndRequestPermissions();
    }

    private void initialize() {
        context = this;
        Utility.crashLytics(context);
        Utility.setStatusColor(this);
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView() {
        imgv_logo = findViewById(R.id.imgv_logo);
        imgv_back = findViewById(R.id.imgv_back);

        edt_email_address = findViewById(R.id.edt_email_address);
        edt_password = findViewById(R.id.edt_password);
        edt_name = findViewById(R.id.edt_name);
        edt_phone = findViewById(R.id.edt_phone);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);

        txt_login = findViewById(R.id.txt_login);
        txt_signup = findViewById(R.id.txt_signup);

    }

    private void setData() {
        //parth123@gmail.com
        //9725121721
        //1to9
    }

    private void setLitionar() {
        txt_login.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
    }

    private void setColor() {

    }

    @Override
    public void onClick(View view) {
        if (view == txt_signup) {
            if (Utility.isNetworkAvailable(context))
                checkValidation();
            else
                Utility.errDialog(context.getResources().getString(R.string.network), context);
        } else if (view == txt_login) {
            onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        Utility.gotoBack(context);
    }

    private void checkValidation() {
        String error = "";

        if (edt_name.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_name);
        } else if (edt_email_address.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_email);
        } else if (!isValidEmail(edt_email_address.getText())) {
            error += "\n" + context.getString(R.string.enter_valid_email);
        } else if (edt_phone.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_phone);
        } else if (edt_phone.getText().toString().trim().length()<10) {
            error += "\n" + context.getString(R.string.enter_valid_phone);
        } else if (edt_password.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_pass);
        } else if (edt_password.getText().toString().trim().length() < 8) {
            error += "\n" + context.getString(R.string.enter_pass_length);
        } else if (edt_confirm_password.getText().toString().trim().isEmpty()) {
            error += "\n" + context.getString(R.string.enter_confirm_pass);
        } else if (!edt_password.getText().toString().trim().equals(edt_confirm_password.getText().toString())) {
            error += "\n" + context.getString(R.string.password_mustbe_same);
        }

        if (error.equals("")) {
            Utility.hideSoftKeyboard(edt_email_address,context);
            callSignup();
        } else {
            Utility.errDialog(error.substring(1), context);
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void callSignup() {
        pd = Utility.showProgressDialog(context);
        apiServer.signupApi(new APIResponse() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.e("Tag -> ", "object -> " + object.toString());

                    edt_name.setText("");
                    edt_confirm_password.setText("");
                    edt_email_address.setText("");
                    edt_password.setText("");
                    edt_phone.setText("");

                    edt_name.requestFocus();

                    Utility.dismissProgressDialog(pd);
                    Utility.errDialog(object.getString("message"),context);
                } catch (Exception e) {
                    Utility.dismissProgressDialog(pd);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                Utility.dismissProgressDialog(pd);
                //errDialogTryAgain(0,false);
            }
        }, edt_name.getText().toString().trim(), edt_email_address.getText().toString().trim() ,edt_password.getText().toString().trim(), edt_confirm_password.getText().toString().trim(), edt_phone.getText().toString().trim());
    }

}
