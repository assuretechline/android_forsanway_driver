package com.forsanway.driver.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.forsanway.driver.R;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


/**
 * Created by home on 12/07/18.
 */

public class TempActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    ImageView imgv_back;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
        initialize();
        setView();
        setData();
        setListener();
        setColor();

    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer=new APIServer(context);
        appPrefrece=new AppPrefrece(context);
    }

    private void setView() {
        imgv_back = findViewById(R.id.imgv_back);

    }

    private void setData() {

    }

    private void setListener() {
        imgv_back.setOnClickListener(this);
    }

    private void setColor() {
        imgv_back.setColorFilter(context.getResources().getColor(R.color.white_image));
    }

    @Override
    public void onClick(View view) {
        if (view == imgv_back) {
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.gotoBack(context);
    }
}
