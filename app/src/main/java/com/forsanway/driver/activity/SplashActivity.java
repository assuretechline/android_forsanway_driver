package com.forsanway.driver.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.forsanway.driver.R;
import com.forsanway.driver.global.MyPreference;
import com.forsanway.driver.global.Utility;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class SplashActivity extends BaseActivity {
    private static int SPLASH_TIME_OUT = 2000;
    Context context;
    ImageView imgv_logo;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialize();
        setView();
        setData();
        setColor();
    }

    private void initialize() {
        context = this;
        Utility.setStatusColor(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    private void setView() {
        imgv_logo = findViewById(R.id.imgv_logo);
    }

    private void setData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

               /* if(MyPreference.getPreferenceValue(context,"loginData")==null || MyPreference.getPreferenceValue(context,"loginData").equals("")){
                    Utility.gotoNext(context, LoginActivity.class);
                }else {
                    Utility.gotoNext(context, MainActivity.class);
                }*/
                Utility.gotoNext(context, MainActivity.class);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void setColor() {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
