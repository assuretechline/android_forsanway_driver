package com.forsanway.driver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.forsanway.driver.R;
import com.forsanway.driver.adapter.DashboardAdapter;
import com.forsanway.driver.adapter.MenuAdapter;
import com.forsanway.driver.api.APIServer;
import com.forsanway.driver.fragment.CarListFragment;
import com.forsanway.driver.fragment.ComplainFragment;
import com.forsanway.driver.fragment.HomeFragment;
import com.forsanway.driver.fragment.PaymentHistoryFragment;
import com.forsanway.driver.fragment.PaymentMethodFragment;
import com.forsanway.driver.fragment.ProfileFragment;
import com.forsanway.driver.fragment.ReviewFragment;
import com.forsanway.driver.fragment.TripDetailFragment;
import com.forsanway.driver.fragment.TripListFragment;
import com.forsanway.driver.fragment.WithdrawFragment;
import com.forsanway.driver.global.MyPreference;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.DashboardData;
import com.forsanway.driver.model.MenuData;
import com.forsanway.driver.sharedPrefrence.AppPrefrece;
import com.google.android.material.navigation.NavigationView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    Context context;
    ImageLoader imageLoader;
    APIServer apiServer;
    AppPrefrece appPrefrece;

    NavigationView nvViewList;
    ListView lstview;
    MenuAdapter menuAdapter;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView imgv_menu;
    ArrayList<MenuData> menuDataList = new ArrayList<>();
    FrameLayout fl_main;
    ProgressDialog pd;
    public static MainActivity mainActivity;

    public static MainActivity getInstance() {
        return mainActivity;
    }

    TextView txt_title;
    String frg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        setView();
        setData();
        setListener();
        setColor();
    }

    private void initialize() {
        context = this;
        mainActivity = this;
        Utility.setStatusColor(this);
        Utility.crashLytics(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        apiServer = new APIServer(context);
        appPrefrece = new AppPrefrece(context);
    }

    private void setView() {
        nvViewList = findViewById(R.id.nvViewList);

        lstview = findViewById(R.id.lstview);
        txt_title = findViewById(R.id.txt_title);
        fl_main = findViewById(R.id.fl_main);

        drawerLayout = findViewById(R.id.drawer_lauout_list);
        navigationView = findViewById(R.id.nvViewList);
        imgv_menu = findViewById(R.id.imgv_menu);
    }

    private void setData() {
        MyPreference.setPreferenceValue(context,"user_id","3");

        menuDataList.add(new MenuData(context.getString(R.string.Dashboard), R.drawable.dashboard, true));
        menuDataList.add(new MenuData(context.getString(R.string.PaymentMethod), R.drawable.credit_card, false));
        menuDataList.add(new MenuData(context.getString(R.string.Car), R.drawable.group, false));
        menuDataList.add(new MenuData(context.getString(R.string.Withdrow), R.drawable.withdraw, false));
        menuDataList.add(new MenuData(context.getString(R.string.PartnerPaymentHistory), R.drawable.handshake, false));
        menuDataList.add(new MenuData(context.getString(R.string.Booking), R.drawable.booking, false));
        menuDataList.add(new MenuData(context.getString(R.string.Review), R.drawable.writing, false));
        menuDataList.add(new MenuData(context.getString(R.string.Complain), R.drawable.phone, false));

        menuAdapter = new MenuAdapter(context, menuDataList);
        lstview.setAdapter(menuAdapter);

       // setFragmentMainTab(new HomeFragment(), "HomeFragment");
        setFragmentMainTab(new HomeFragment(), "HomeFragment");
    }

    private void setListener() {
        imgv_menu.setOnClickListener(this);

        lstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> menuAdapterView, View view, int i, long l) {

                for(int j=0;j<menuDataList.size();j++){
                    menuDataList.get(j).setSelected(false);
                }
                menuDataList.get(i).setSelected(true);
                menuAdapter.notifyDataSetChanged();

                txt_title.setText(menuDataList.get(i).getTitle());

                if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.Dashboard))) {
                    setFragmentMainTab(new HomeFragment(), "HomeFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.PaymentMethod))) {
                    setFragmentMainTab(new PaymentMethodFragment(), "PaymentMethodFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.Car))) {
                    setFragmentMainTab(new CarListFragment(), "CarListFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.Withdrow))) {
                    setFragmentMainTab(new WithdrawFragment(), "WithdrawFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.PartnerPaymentHistory))) {
                    setFragmentMainTab(new PaymentHistoryFragment(), "PaymentHistoryFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.Booking))) {
                    setFragmentMainTab(new TripListFragment(), "TripListFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.Review))) {
                    setFragmentMainTab(new ReviewFragment(), "ReviewFragment");
                }else if (menuDataList.get(i).getTitle().equalsIgnoreCase(context.getString(R.string.Complain))) {
                    setFragmentMainTab(new ComplainFragment(), "ComplainFragment");
                }
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        });
    }

    private void setColor() {
        imgv_menu.setColorFilter(context.getResources().getColor(R.color.black_image));
    }

    @Override
    public void onClick(View view) {
        if(view==imgv_menu){
            setDrawer();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        setBackPressed();
    }


    public void setFragmentMainTab(Fragment fragment, String tag) {
        if (tag.equals(frg)) {
        } else {
            frg = tag;
            getSupportFragmentManager().beginTransaction().replace(fl_main.getId(), fragment, tag).addToBackStack(tag).commitAllowingStateLoss();
        }
    }

    public void setBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }else {
            int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
            FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
            String tag = backEntry.getName();
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);

            if (fragment.getClass().equals(HomeFragment.class)) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    public void setDrawer() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {

        } catch (Exception e) {

        }
    }
}

