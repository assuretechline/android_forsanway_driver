package com.forsanway.driver.sharedPrefrence;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by acer on 30-06-2017.
 */
public class AppPrefrece {
    SharedPreferences.Editor edt;
    String AppPrefrece = "AppPrefrece";
    String UserData = "UserData";
    String ISLOGIN = "isLogin";
    String USER_ID = "user_id";
    String ACCESS_TOKEN = "access_token";
    SharedPreferences sp;

    public AppPrefrece(Context context) {
        sp = context.getSharedPreferences(AppPrefrece, Context.MODE_PRIVATE);
        edt = sp.edit();
    }

    public void remove() {
        edt.remove(ISLOGIN);
        edt.remove(USER_ID);
        edt.remove(ACCESS_TOKEN);
        edt.commit();
    }

    public JSONObject getProfile() throws JSONException {
        String strJson = sp.getString(UserData, "0");//second parameter is necessary ie.,Value to return if this preference does not exist.
        JSONObject jsonData = new JSONObject(strJson);
        return jsonData;
    }

    public void setProfile(JSONObject profile) {
        edt.putString(UserData, profile.toString());
        edt.commit();
    }

    public String getUserId() {
        return sp.getString(USER_ID, "");
    }

    public void setUserId(String user_id) {
        edt.putString(USER_ID, user_id);
        edt.commit();
    }

    public Boolean getLogin() {
        return sp.getBoolean(ISLOGIN, true);
    }

    public void setLogin(Boolean login) {
        edt.putBoolean(ISLOGIN, login);
        edt.commit();
    }

    public String getAccessToken() {
        return sp.getString(ACCESS_TOKEN, "");
    }

    public void setAccessToken(String siteToken) {
        edt.putString(ACCESS_TOKEN, siteToken);
        edt.commit();

    }

}
