package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.forsanway.driver.R;
import com.forsanway.driver.model.CarListData;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class TripDetailAdapter extends BaseAdapter {
    Context context;
    ArrayList<CarListData> list;

    public TripDetailAdapter(Context context, ArrayList<CarListData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_trip_detail, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_user_payal = view.findViewById(R.id.txt_user_payal);
        holder.txt_trip = view.findViewById(R.id.txt_trip);
        holder.imgv_delete = view.findViewById(R.id.imgv_delete);
        holder.txt_price = view.findViewById(R.id.txt_price);
        holder.txt_passanger = view.findViewById(R.id.txt_passanger);
        holder.txt_payment_status = view.findViewById(R.id.txt_payment_status);
        holder.txt_booking_status = view.findViewById(R.id.txt_booking_status);

        holder.ll_bottom = view.findViewById(R.id.ll_bottom);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
       /* holder.txt_user_payal.setText(list.get(position).getTitle());
        holder.txt_trip.setText(list.get(position).getModel());
        holder.txt_passanger.setText(list.get(position).getNumber());
        holder.txt_payment_status.setText(list.get(position).getService());
        holder.txt_price.setText(list.get(position).getPerson() +" "+context.getResources().getString(R.string.Person)+", "+list.get(position).getBag()+" "+context.getResources().getString(R.string.Bag));
*/

        if(list.get(position).isSelected()){
            holder.ll_bottom.setBackgroundColor(context.getResources().getColor(R.color.blue));
            holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.blue));
        }else {
            holder.ll_bottom.setBackgroundColor(context.getResources().getColor(R.color.grey_font));
            holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.grey_font));
        }
    }


    private void initialize() {
    }

    private class Holder {
        TextView txt_user_payal,txt_trip,txt_price,txt_passanger,txt_payment_status,txt_booking_status;
        ImageView imgv_delete;
        LinearLayout ll_bottom;
    }
}
