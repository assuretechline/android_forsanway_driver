package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.forsanway.driver.R;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by acer on 12-08-2017.
 */
public class PaymentHistoryAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<CarListData> list;

    public PaymentHistoryAdapter(Context context, ArrayList<CarListData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_payment_history, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_name = view.findViewById(R.id.txt_name);
        holder.txt_city = view.findViewById(R.id.txt_city);
        holder.imgv_payment = view.findViewById(R.id.imgv_payment);
        holder.txt_amount = view.findViewById(R.id.txt_amount);
        holder.txt_seat = view.findViewById(R.id.txt_seat);
        holder.txt_date = view.findViewById(R.id.txt_date);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        holder.txt_name.setText(list.get(position).getTitle());
        holder.txt_city.setText(list.get(position).getModel());
        holder.txt_seat.setText("06 Seat Booked");
        holder.txt_date.setText(list.get(position).getService());
        holder.txt_amount.setText("$2800");

        imageLoader.displayImage(list.get(position).getImage(),holder.imgv_payment,Utility.getImageOptions());

    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private static class Holder {
        TextView txt_name,txt_city,txt_amount,txt_seat,txt_date;
        ImageView imgv_payment;
    }
}
