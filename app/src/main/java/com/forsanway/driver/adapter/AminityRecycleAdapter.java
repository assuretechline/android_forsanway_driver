package com.forsanway.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.forsanway.driver.R;
import com.forsanway.driver.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class AminityRecycleAdapter extends RecyclerView.Adapter<AminityRecycleAdapter.ViewHolder> {
    Context context;
    ImageLoader imageLoader;
    ArrayList<MenuData> list;
    int sdk ;

    public AminityRecycleAdapter(Context context, ArrayList<MenuData> menuDataList) {
        this.context = context;
        this.list = menuDataList;
        initialize();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_aminity, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        setData(holder, position);

    }

    public void setData(ViewHolder holder, final int position) {
        holder.imgv_icon.setImageResource(list.get(position).getImage());

        if(!list.get(position).isSelected()){
            holder.imgv_icon.setColorFilter(context.getResources().getColor(R.color.grey_font));
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                holder.ll_main.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.round_corner_grey_white_10) );
            } else {
                holder.ll_main.setBackground(ContextCompat.getDrawable(context, R.drawable.round_corner_grey_white_10));
            }
        }else{
            holder.imgv_icon.setColorFilter(context.getResources().getColor(R.color.white_font));
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                holder.ll_main.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.round_corner_green_white_10) );
            } else {
                holder.ll_main.setBackground(ContextCompat.getDrawable(context, R.drawable.round_corner_green_white_10));
            }
        }


        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list.get(position).isSelected()){
                    list.get(position).setSelected(false);
                }else {
                    list.get(position).setSelected(true);
                }
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgv_icon;
        LinearLayout ll_main;

        public ViewHolder(View view) {
            super(view);
            imgv_icon = view.findViewById(R.id.imgv_icon);
            ll_main = view.findViewById(R.id.ll_main);
        }
    }

    private void initialize() {
        sdk = android.os.Build.VERSION.SDK_INT;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

}
