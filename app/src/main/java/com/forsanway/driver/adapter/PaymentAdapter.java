package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.forsanway.driver.R;
import com.forsanway.driver.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class PaymentAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<MenuData> list;

    public PaymentAdapter(Context context, ArrayList<MenuData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_payment, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
        holder.imgv_icon = view.findViewById(R.id.imgv_icon);
        holder.ll_main = view.findViewById(R.id.ll_main);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        holder.txt_title.setText(list.get(position).getTitle());
        holder.imgv_icon.setImageResource(list.get(position).getImage());

        if(list.get(position).isSelected()){
            holder.ll_main.setBackground(ContextCompat.getDrawable(context, R.drawable.round_corner_fill_dark_blue_7));
            holder.txt_title.setTextColor(context.getResources().getColor(R.color.white_font));
        }else {
            holder.ll_main.setBackground(ContextCompat.getDrawable(context, R.drawable.round_corner_grey_border_7));
            holder.txt_title.setTextColor(context.getResources().getColor(R.color.dark_grey_font));
        }

    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private class Holder {
        TextView txt_title;
        ImageView imgv_icon;
        LinearLayout ll_main;
    }
}
