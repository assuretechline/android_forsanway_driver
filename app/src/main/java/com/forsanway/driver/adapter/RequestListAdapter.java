package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.forsanway.driver.R;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by acer on 12-08-2017.
 */
public class RequestListAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<CarListData> list;

    public RequestListAdapter(Context context, ArrayList<CarListData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_request_list, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_date = view.findViewById(R.id.txt_date);
        holder.txt_index = view.findViewById(R.id.txt_index);
        holder.imgv_delete = view.findViewById(R.id.imgv_delete);
        holder.txt_type = view.findViewById(R.id.txt_type);
        holder.txt_amount = view.findViewById(R.id.txt_amount);
        holder.ll_bottom = view.findViewById(R.id.ll_bottom);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        holder.txt_date.setText(list.get(position).getTitle());
        holder.txt_index.setText( ((position+1) < 10 ? "0" : "") + (position+1));
        holder.txt_amount.setText(list.get(position).getNumber());
        holder.txt_type.setText(list.get(position).getService());

        if(list.get(position).isSelected()){
            holder.ll_bottom.setBackgroundColor(context.getResources().getColor(R.color.blue));
            holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.blue));
        }else {
            holder.ll_bottom.setBackgroundColor(context.getResources().getColor(R.color.grey_font));
            holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.grey_font));
        }

    }


    private void initialize() {
    }

    private static class Holder {
        TextView txt_date,txt_index,txt_type,txt_amount;
        ImageView imgv_delete;
        LinearLayout ll_bottom;
    }
}
