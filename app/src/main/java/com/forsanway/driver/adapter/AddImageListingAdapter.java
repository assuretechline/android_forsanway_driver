package com.forsanway.driver.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.forsanway.driver.R;
import com.forsanway.driver.model.AddImageListingData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by home on 07/07/18.
 */

public class AddImageListingAdapter extends RecyclerView.Adapter<AddImageListingAdapter.HorizontalViewHolder> {

    public ArrayList<AddImageListingData> list;
    Context context;
    ImageLoader imageLoader;
    ProgressDialog pd;


    public AddImageListingAdapter(Context context, ArrayList<AddImageListingData> list) {
        this.list = list;
        this.context = context;
        initialize();
    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public HorizontalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_add_image_listing, parent, false);
        return new HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HorizontalViewHolder holder, final int position) {
        setData(holder, position);
    }

    public void setData(final HorizontalViewHolder holder, final int position) {

        holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.white_image));

        //Log.e("Tag","Image load "+list.get(position).getImgLoad());

        ImageLoader.getInstance().displayImage(list.get(position).getImgLoad(), holder.imgv_prop);

        holder.imgv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultDialogWithYesNo("Are you sure?", context, position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HorizontalViewHolder extends RecyclerView.ViewHolder {

        ImageView imgv_prop, imgv_delete;
        LinearLayout ll_main;

        public HorizontalViewHolder(View itemView) {
            super(itemView);
            imgv_prop = itemView.findViewById(R.id.imgv_prop);
            imgv_delete = itemView.findViewById(R.id.imgv_delete);
            ll_main = itemView.findViewById(R.id.ll_main);
        }
    }

    public void defaultDialogWithYesNo(String error, Context context, final int position) {
        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
        alertDialog2.setTitle("Alert!");
        alertDialog2.setMessage(error);

        alertDialog2.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        list.remove(position);
                        notifyDataSetChanged();
                        dialog.cancel();
                    }
                });

        alertDialog2.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });

        alertDialog2.show();
    }
}