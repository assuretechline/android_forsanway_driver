package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chaek.android.RatingBar;
import com.forsanway.driver.R;
import com.forsanway.driver.global.Utility;
import com.forsanway.driver.model.CarListData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by acer on 12-08-2017.
 */
public class CarListAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<CarListData> list;

    public CarListAdapter(Context context, ArrayList<CarListData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_car_list, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
        holder.txt_model = view.findViewById(R.id.txt_model);
        holder.imgv_car = view.findViewById(R.id.imgv_car);
        holder.imgv_edit = view.findViewById(R.id.imgv_edit);
        holder.imgv_delete = view.findViewById(R.id.imgv_delete);
        holder.txt_person_bag = view.findViewById(R.id.txt_person_bag);
        holder.txt_car_no = view.findViewById(R.id.txt_car_no);
        holder.txt_service = view.findViewById(R.id.txt_service);

        holder.ll_bottom = view.findViewById(R.id.ll_bottom);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        holder.txt_title.setText(list.get(position).getTitle());
        holder.txt_model.setText(list.get(position).getModel());
        holder.txt_car_no.setText(list.get(position).getNumber());
        holder.txt_service.setText(list.get(position).getService());
        holder.txt_person_bag.setText(list.get(position).getPerson() +" "+context.getResources().getString(R.string.Person)+", "+list.get(position).getBag()+" "+context.getResources().getString(R.string.Bag));

        imageLoader.displayImage(list.get(position).getImage(),holder.imgv_car,Utility.getImageOptions());

        if(list.get(position).isSelected()){
            holder.ll_bottom.setBackgroundColor(context.getResources().getColor(R.color.blue));
            holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.blue));
            holder.imgv_edit.setColorFilter(context.getResources().getColor(R.color.blue));
        }else {
            holder.ll_bottom.setBackgroundColor(context.getResources().getColor(R.color.grey_font));
            holder.imgv_delete.setColorFilter(context.getResources().getColor(R.color.grey_font));
            holder.imgv_edit.setColorFilter(context.getResources().getColor(R.color.grey_font));
        }

    }


    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private static class Holder {
        TextView txt_title,txt_model,txt_person_bag,txt_car_no,txt_service;
        CircleImageView imgv_car;
        ImageView imgv_edit,imgv_delete;
        LinearLayout ll_bottom;
    }
}
