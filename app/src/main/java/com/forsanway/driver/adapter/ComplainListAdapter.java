package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.forsanway.driver.R;
import com.forsanway.driver.model.CarListData;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class ComplainListAdapter extends BaseAdapter {
    Context context;
    ArrayList<CarListData> list;

    public ComplainListAdapter(Context context, ArrayList<CarListData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_complain_list, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_trip_id = view.findViewById(R.id.txt_trip_id);
        holder.txt_trip = view.findViewById(R.id.txt_trip);
        holder.txt_rating_from = view.findViewById(R.id.txt_rating_from);
        holder.txt_rating = view.findViewById(R.id.txt_rating);
        holder.txt_reply = view.findViewById(R.id.txt_reply);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {
        /*holder.txt_trip_id.setText(list.get(position).getTitle());
        holder.txt_trip.setText(list.get(position).getModel());
        holder.txt_rating.setText(list.get(position).getNumber());
        holder.txt_trip_type.setText(list.get(position).getService());
        holder.txt_rating_from.setText(list.get(position).getPerson() +" "+context.getResources().getString(R.string.Person)+", "+list.get(position).getBag()+" "+context.getResources().getString(R.string.Bag));*/


        if(list.get(position).isSelected()){
            holder.txt_reply.setBackground(ContextCompat.getDrawable(context, R.drawable.round_corner_blue_15));
        }else {
            holder.txt_reply.setBackground(ContextCompat.getDrawable(context, R.drawable.round_corner_grey_15));
        }
    }


    private void initialize() {
    }

    private class Holder {
        TextView txt_trip_id,txt_trip,txt_rating_from,txt_rating,txt_reply;
    }
}
