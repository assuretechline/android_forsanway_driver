package com.forsanway.driver.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.forsanway.driver.R;
import com.forsanway.driver.model.MenuData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class TempRecycleAdapter extends RecyclerView.Adapter<TempRecycleAdapter.ViewHolder> {
    Context context;
    ImageLoader imageLoader;
    ArrayList<MenuData> list;


    public TempRecycleAdapter(Context context, ArrayList<MenuData> menuDataList) {
        this.context = context;
        this.list = menuDataList;
        initialize();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        setData(holder, position);

    }

    public void setData(ViewHolder holder, final int position) {
        holder.imgv_icon.setImageResource(list.get(position).getImage());
        holder.txt_title.setText(list.get(position).getTitle());

      /*  if(list.get(position).getTitle().equalsIgnoreCase("Students")){
            holder.txt_count.setTextColor(context.getResources().getColor(R.color.theme));
        }else{
            holder.txt_count.setTextColor(context.getResources().getColor(R.color.black));
        }*/


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgv_icon;
        TextView txt_title;
        LinearLayout ll_main;

        public ViewHolder(View view) {
            super(view);
            imgv_icon = view.findViewById(R.id.imgv_icon);
            txt_title = view.findViewById(R.id.txt_title);
            ll_main = view.findViewById(R.id.ll_main);
        }
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

}
