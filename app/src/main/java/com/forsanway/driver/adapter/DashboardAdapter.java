package com.forsanway.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.forsanway.driver.R;
import com.forsanway.driver.model.DashboardData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by acer on 12-08-2017.
 */
public class DashboardAdapter extends BaseAdapter {
    ImageLoader imageLoader;
    Context context;
    ArrayList<DashboardData> list;

    public DashboardAdapter(Context context, ArrayList<DashboardData> list) {
        this.context = context;
        this.list = list;
        initialize();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_dashboard, null);
            setView(convertView, holder);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();
        setData(holder, position);
        return convertView;
    }

    private void setView(View view, Holder holder) {
        holder.txt_title = view.findViewById(R.id.txt_title);
        holder.txt_count = view.findViewById(R.id.txt_count);
        holder.imgv_icon = view.findViewById(R.id.imgv_icon);
    }

    @SuppressLint("NewApi")
    private void setData(Holder holder, int position) {

        holder.txt_title.setTextColor(list.get(position).getColor());
        holder.txt_title.setText(list.get(position).getTitle());
        holder.txt_count.setText(list.get(position).getCount());
        holder.imgv_icon.setImageResource(list.get(position).getImage());
    }

    private void initialize() {
        imageLoader = ImageLoader.getInstance();
    }

    private class Holder {
        TextView txt_title,txt_count;
        ImageView imgv_icon;
    }
}
